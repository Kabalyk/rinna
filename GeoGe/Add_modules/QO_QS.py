#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 21:46:37 2019

@author: kirill_DUO
"""
""" Rinna
    Copyright (C) 2019  Kirill Kabalyk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact e-mail: kirill.kabalyk@gmail.com    
 """



""" This module contains functions to compute quantities for quasi-orthogonals and quasi-streamlines """

import math
from numpy import array as ar
from matplotlib import pyplot as plt
from numpy import cos as c
from numpy import sin as s
from numpy import sqrt as sqrt
from numpy import sum as summ
from numpy import around as around

# Function QO_length computes length of QO
def QO_length(Zs, Zh, Rs, Rh):
    QO_L = math.sqrt((Zs - Zh)**2 + (Rs - Rh)**2)
    QO_length.QO_L = QO_L
    return QO_L

# Function QO_ax_inc computes meridional axial inclination of QO 
def QO_ax_inc(Zs, Zh, Rs, Rh):
    dZ = abs(Zs - Zh)
    dR = abs(Rs - Rh)
    inc = math.atan(dR/dZ)
    QO_ax_inc.inc = inc
    return inc
    
# Function Inter_QS_nodes computes coords of basic nodes of intermediate QS 
def Inter_QS_nodes(SBN_Z, SBN_R, QOL, inc_QO, z_IQSo, z_IQSc):
    delta_QOL = QOL/(z_IQSo-1)
    IQSN_Z = []
    for y in range(0, len(SBN_Z)):
        if SBN_Z[y] >= 0:
            IQSN_Z_y = SBN_Z[y] - delta_QOL[y] * (z_IQSc - 1) * c(inc_QO[y])
        else:
            IQSN_Z_y = SBN_Z[y] + delta_QOL[y] * (z_IQSc - 1) * c(inc_QO[y])
        IQSN_Z.append(IQSN_Z_y)
    IQSN_R = SBN_R - delta_QOL * (z_IQSc - 1) * s(inc_QO)
#    plt.plot(IQSN_Z, IQSN_R, "bo")
    return around(IQSN_Z, decimals = 3), around(IQSN_R, decimals = 3)

# Function QS_mer_length computes meridional length of a given piece of QS, current dimensional and dimensionless meridional
# length at each point of the selected piece    
def QS_mer_length(m_st, m_fin, Z, R, dtype = "float64"):
    Z_SP = []
    R_SP =  []
    QSM_dZ = []
    QSM_dR = []
    QSM_dL = []
    QSM_Li = []
    for k in range(m_st, m_fin + 1):  #try if elif
        Z_SP_k = Z[k]
        R_SP_k = R[k]
        QSM_dZ_k = Z[k+1]-Z[k]
        QSM_dR_k = R[k+1]-R[k]
        QSM_dL_k = sqrt(QSM_dZ_k**2 + QSM_dR_k**2)
        QSM_Li_k = summ(QSM_dL)
        Z_SP.append(round(Z_SP_k, 3))
        R_SP.append(round(R_SP_k, 3))
        QSM_dZ.append(round(QSM_dZ_k, 3))
        QSM_dR.append(round(QSM_dR_k, 3))
        QSM_dL.append(round(QSM_dL_k, 3))
        QSM_Li.append(round(QSM_Li_k, 3))
    QSM_L = summ(QSM_dL)
    _QSM_Li = QSM_Li / QSM_L
    return (around(Z_SP, decimals = 3), around(R_SP, decimals = 3), around(QSM_dZ, decimals = 3), around(QSM_dR, decimals = 3), 
            around(QSM_dL, decimals = 3), around(QSM_Li, decimals = 3), around(_QSM_Li, decimals = 3), round(QSM_L, 3))
        





















