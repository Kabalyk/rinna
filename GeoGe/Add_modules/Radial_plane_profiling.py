#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 20:00:28 2019

@author: kirill_DUO
"""

""" Rinna
    Copyright (C) 2019  Kirill Kabalyk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact e-mail: kirill.kabalyk@gmail.com    
 """



""" This module contains functions to compute blade/vane profiles in radial (blade-to-blade) plane """

from numpy import array as ar
from numpy import concatenate as conc
from numpy import sin as sin
from numpy import cos as cos
from numpy import pi as pi
from numpy import linspace as linspace
from numpy import arcsin as asin
from numpy import around as around
import matplotlib.pyplot as plt
import random as ran
from matplotlib import rc
 
# Function "imp_rad_prof_LPI" returns blade/vane midline profile using simplified Leningrad Politechnic Institute approach [1]

def imp_rad_prof_LPI(beta_b1, beta_b2, R2, R, A, B):
    #_R = conc((ar(R)/R2, [1.0]))
    _R = ar(R)/R2
    _R_infl = B*(1 - _R[0])+_R[0]
    beta_infl = A*(beta_b2-beta_b1)+beta_b1
    beta_b = []
    for f in range(0, len(_R)):
        if _R[f] < _R_infl:
            beta_b_i = beta_b1+A*(beta_b2-beta_b1)*(1-(1-(_R[f]-_R[0])/(B*(1-_R[0])))**2)
        elif _R[f] > _R_infl:
            beta_b_i = beta_b1+A*(beta_b2-beta_b1)*(1-(1-1/A)*(((_R[f]-_R[0])-B*(1-_R[0]))/((1-B)*(1-_R[0])))**2)
        else:
            beta_b_i = beta_infl
        beta_b.append(round(beta_b_i, 3))
#    font_tit = {'family': 'serif',
#        'color':  'darkred',
#        'weight': 'normal',
#        'size': 15,
#        }
#    font_ax = {'family': 'serif',
#        'color':  'black',
#        'weight': 'normal',
#        'size': 12,
#        }
#    plt.figure()
#    plt.plot(_R, beta_b, "bs")
#    plt.title("Blade angle Vs Radius", fontdict = font_tit)
#    plt.xlabel("Radius", fontdict = font_ax)
#    plt.ylabel("Beta_(b)", fontdict = font_ax)
    return around(_R, decimals = 3), around(beta_b, decimals = 1)

# Function "imp_rad_prof_arc" returns blade/vane midline single-arc profile

def imp_rad_prof_arc(beta_b1, beta_b2, R):
    return


# Function "rad_prof_sketch" returns the blade profile midline in xy plane

def rad_prof_sketch(R, R2, beta_b, dl_mer, fi0):
    R_full = conc((ar(R), [R2]))
    dl_rad0 = dl_mer[0]/sin(beta_b[0]*pi/180)
    x0 = R[0]*cos(fi0*pi/180)
    y0 = R[0]*sin(fi0*pi/180)
    dl_rad = [round(dl_rad0, 3)]
    fi_circ = linspace(0, pi/2, 90)
    fi = [fi0]
    x = [round(x0,3)]
    y = [round(y0,3)]
    for b in range(1, len(R)):
        fi_b = asin((dl_rad[b-1]*sin((beta_b[b-1]-90+fi[b-1])*pi/180)+R_full[b-1]*sin(fi[b-1]*pi/180))/R_full[b])*180/pi
#        while True:
#            fi_b = round(ran.uniform(fi0-100, fi0), 4)
#            Q = round(R_full[b]*sin(fi_b*pi/180) - R_full[b-1]*sin(fi[b-1]*pi/180),4)
#            M = round(dl_rad[b-1]*sin((beta_b[b-1]-90+fi[b-1])*pi/180),4)
#            if abs(Q-M)<=0.1:
#                break
        dl_rad.append(round(dl_mer[b]/sin(beta_b[b]*pi/180), 3))    
        fi.append(round(fi_b, 3))
        x.append(round(R_full[b]*cos(fi_b*pi/180), 3))
        y.append(round(R_full[b]*sin(fi_b*pi/180), 3))
#    font_tit = {'family': 'serif',
#        'color':  'darkred',
#        'weight': 'normal',
#        'size': 15,
#        }
#    font_ax = {'family': 'serif',
#        'color':  'black',
#        'weight': 'normal',
#        'size': 12,
#        }    
#    plt.figure(figsize=(7,7))
#    plt.plot(x,y, "^r")
#    plt.plot(R_full[0]*cos(fi_circ), R_full[0]*sin(fi_circ), "g")
#    plt.plot(R2*cos(fi_circ), R2*sin(fi_circ), "g")
#    plt.title("Impeller blade midline sketch, B2B plane", fontdict = font_tit)
#    plt.xlabel("X", fontdict = font_ax)
#    plt.ylabel("Y", fontdict = font_ax)
    return (around(dl_rad, decimals = 3), around(fi, decimals = 1), around(x, decimals = 3), 
            around(y, decimals = 3), around(R_full, decimals = 3), around(fi_circ, decimals = 3)) 




# References:
# 1. Проектирование и оптимизация проточной части промышленных центробежных
#    компрессоров с использованием ЭВМ /К.П. Селезнев, Ю.Б. Галеркин, Б.Н. Савин,
#    Е.Ю. Попова, Р .А. Измайлов; Ленингр. гос . техн. ун-т. Л.,1990.
    