#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 18:40:44 2019

@author: kirill_DUO
"""

""" Rinna
    Copyright (C) 2019  Kirill Kabalyk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact e-mail: kirill.kabalyk@gmail.com
    
 """



""" This module contains functions for computing the quasi-streamlines' coords in meridional plane """

import math
import numpy as np
import bezier
import matplotlib.pyplot as plt

# Function imp_strm_line computes coordinates of a single impeller quasi-streamline: 

def imp_strm_line(Z0, Z1, Z2, Z3, Z4, R0, R1, R2, R3, R4, kR, kZ, z_Bez, z_int2D):
    ISL_nodes = np.asfortranarray([[-Z3, -kZ*(Z3+Z2), -Z2, -Z2],
                                     [R3, R3, kR*(R3+R2), R2]], dtype = "float64")
    ISL_crv = bezier.Curve(ISL_nodes, degree = 3)
    s_vec = np.linspace(0,1,z_Bez)
    ISL_crv_coords = ISL_crv.evaluate_multi(s_vec)
    ISL_crv_Z_coords = np.compress([True, False], ISL_crv_coords, axis = 0)
    ISL_crv_R_coords = np.compress([False, True], ISL_crv_coords, axis = 0)
    Z_int_2D_st = abs(Z2-Z1)/z_int2D
    R_int_2D_st = abs(R1-R2)/z_int2D
    if abs(Z2-Z1)>0:
        Z_int_2D = np.linspace(-Z2+Z_int_2D_st, -Z1-Z_int_2D_st, z_int2D-1)
    else:
        Z_int_2D = np.zeros(z_int2D-1)       
    ISL_Z = np.concatenate((np.array([-Z4]), ISL_crv_Z_coords, Z_int_2D, np.array([-Z1, -Z0])), axis=None)
    ISL_R = np.concatenate((np.array([R4]), ISL_crv_R_coords, np.linspace(R2+R_int_2D_st, R1-R_int_2D_st, z_int2D-1), np.array([R1, R0])), axis=None)
    ISL_BN_Z = np.array([-Z4, -Z3, -Z2, -Z1, -Z0]) 
    ISL_BN_R = np.array([R4, R3, R2, R1, R0])
    imp_strm_line.ISL_Z = ISL_Z
    imp_strm_line.ISL_R = ISL_R
    imp_strm_line.ISL_BN_Z = ISL_BN_Z
    imp_strm_line.ISL_BN_R = ISL_BN_R
#    font_tit = {'family': 'serif',
#        'color':  'darkred',
#        'weight': 'normal',
#        'size': 15,
#        }
#    font_ax = {'family': 'serif',
#        'color':  'black',
#        'weight': 'normal',
#        'size': 12,
#        }
#    plt.plot(ISL_Z, ISL_R, "go-")
#    plt.title("Impeller quasi-streamlines, meridional sketch", fontdict = font_tit)
#    plt.xlabel("Axial distance", fontdict = font_ax)
#    plt.ylabel("Radius", fontdict = font_ax)
    return np.around(ISL_Z, decimals = 3), np.around(ISL_R, decimals = 3), np.around(ISL_BN_Z, decimals = 3), np.around(ISL_BN_R, decimals = 3)

# Function dif_strm_line computes coordinates of a single radial diffuser quasi-streamline:

def dif_strm_line(Z0D, Z1D, Z2D, Z3D, R0D, R1D, R2D, R3D, kRD, kZD, zD_Bez, zD_int2D):
    DSL_Bnodes = np.asfortranarray([[-Z3D, -kZD*(Z3D+Z2D), -Z2D, -Z2D],
                                     [R3D, R2D, kRD*(R3D+R2D), R2D]], dtype = "float64")
    DSL_Bcrv = bezier.Curve(DSL_Bnodes, degree = 3)
    s_Bvec = np.linspace(0, 1, zD_Bez)
    DSL_Bcrv_coords = DSL_Bcrv.evaluate_multi(s_Bvec)
    DSL_Bcrv_Z_coords = np.compress([True, False], DSL_Bcrv_coords, axis = 0)
    DSL_Bcrv_R_coords = np.compress([False, True], DSL_Bcrv_coords, axis = 0)
    Z_int_2DD_st = abs(Z2D-Z1D)/zD_int2D
    R_int_2DD_st = abs(R1D-R2D)/zD_int2D
    if abs(Z2D-Z1D)>0:   
        Z_int_2DD = np.linspace(-Z2D+Z_int_2DD_st, -Z1D-Z_int_2DD_st, zD_int2D-1)
    else:
        Z_int_2DD = np.linspace(-Z2D, -Z2D, zD_int2D-1)
    DSL_Z = np.concatenate((DSL_Bcrv_Z_coords, Z_int_2DD, np.array([-Z1D, -Z0D])), axis = None)
    DSL_R = np.concatenate((DSL_Bcrv_R_coords, np.linspace(R2D+R_int_2DD_st, R1D-R_int_2DD_st, zD_int2D-1), np.array([R1D, R0D])), axis = None)
    DSL_BN_Z = np.array([-Z3D, -Z2D, -Z1D, -Z0D])
    DSL_BN_R = np.array([R3D, R2D, R1D, R0D])
#    font_tit = {'family': 'serif',
#        'color':  'darkred',
#        'weight': 'normal',
#        'size': 15,
#        }
#    font_ax = {'family': 'serif',
#        'color':  'black',
#        'weight': 'normal',
#        'size': 12,
#        }
#    plt.plot(DSL_Z, DSL_R, "ro-")
#    plt.title("Diffuser quasi-streamlines, meridional sketch", fontdict = font_tit)
#    plt.xlabel("Axial distance", fontdict = font_ax)
#    plt.ylabel("Radius", fontdict = font_ax)    
#    plt.xlim(-0.3*R3D, 0)
#    plt.ylim(0, R0D)
    return (np.around(DSL_Z, decimals = 3), np.around(DSL_R, decimals = 3), 
            np.around(DSL_BN_Z, decimals = 3), np.around(DSL_BN_R, decimals = 3))

# Function RC_strm_line computes coordinates of a single return channel (with crossover) quasi-streamline:

def RC_strm_line(Z0RC, Z1RC, Z2RC, Z3RC, Z4RC, Z5RC, Z6RC, R0RC, R1RC, R2RC, R3RC, R4RC, R5RC, R6RC,
                 kRRC12, kZRC12, kRRC34, kZRC34, kRRC56, kZRC56, zRC_Bez, zRC_int2D):
    RCSL_Bnodes12 = np.asfortranarray([[ Z2RC, Z2RC, kZRC12*(Z2RC+Z1RC), Z1RC], 
                                        [R2RC, kRRC12*(R2RC+R1RC), R1RC, R1RC]], dtype = "float64")
    RCSL_Bcrv12 = bezier.Curve(RCSL_Bnodes12, degree = 3)
    s_Bvec12 = np.linspace(0, 1, zRC_Bez)
    RCSL_Bcrv12_coords = RCSL_Bcrv12.evaluate_multi(s_Bvec12)
    RCSL_Bcrv12_Z_coords = np.compress([True, False], RCSL_Bcrv12_coords, axis = 0)
    RCSL_Bcrv12_R_coords = np.compress([False, True], RCSL_Bcrv12_coords, axis = 0)
    RCSL_Bnodes34 = np.asfortranarray([[ Z4RC, kZRC34*(Z4RC+Z3RC), Z3RC, Z3RC], 
                                        [R4RC, R4RC, kRRC34*(R4RC+R3RC), R3RC]], dtype = "float64")
    RCSL_Bcrv34 = bezier.Curve(RCSL_Bnodes34, degree = 3)
    s_Bvec34 = np.linspace(0, 1, zRC_Bez)
    RCSL_Bcrv34_coords = RCSL_Bcrv34.evaluate_multi(s_Bvec34)
    RCSL_Bcrv34_Z_coords = np.compress([True, False], RCSL_Bcrv34_coords, axis = 0)
    RCSL_Bcrv34_R_coords = np.compress([False, True], RCSL_Bcrv34_coords, axis = 0)
    RCSL_Bnodes56 = np.asfortranarray([[ Z6RC, Z6RC, kZRC56*(Z6RC+Z5RC), Z5RC], 
                                        [R6RC, kRRC56*(R6RC+R5RC), R5RC, R5RC]], dtype = "float64")
    RCSL_Bcrv56 = bezier.Curve(RCSL_Bnodes56, degree = 3)
    s_Bvec56 = np.linspace(0, 1, zRC_Bez)
    RCSL_Bcrv56_coords = RCSL_Bcrv56.evaluate_multi(s_Bvec56)
    RCSL_Bcrv56_Z_coords = np.compress([True, False], RCSL_Bcrv56_coords, axis = 0)
    RCSL_Bcrv56_R_coords = np.compress([False, True], RCSL_Bcrv56_coords, axis = 0)
    Z_int_2DRC_st = abs(Z3RC-Z2RC)/zRC_int2D
    R_int_2DRC_st = abs(R3RC-R2RC)/zRC_int2D
    if abs(Z3RC-Z2RC)>0:
        Z_int_2DRC = np.linspace(Z3RC+Z_int_2DRC_st, Z2RC-Z_int_2DRC_st, zRC_int2D-1)
    else:
        Z_int_2DRC = np.linspace(Z2RC, Z2RC, zRC_int2D - 1)
    RCSL_Z = np.concatenate((RCSL_Bcrv56_Z_coords, RCSL_Bcrv34_Z_coords, Z_int_2DRC, RCSL_Bcrv12_Z_coords, np.array([Z0RC])), axis = None)
    RCSL_R = np.concatenate((RCSL_Bcrv56_R_coords, RCSL_Bcrv34_R_coords, np.linspace(R3RC-R_int_2DRC_st, R2RC+R_int_2DRC_st, zRC_int2D-1),
                             RCSL_Bcrv12_R_coords, np.array([R0RC])), axis = None)
    RCSL_BN_Z = np.array([Z6RC, Z5RC, Z4RC, Z3RC, Z2RC, Z1RC, Z0RC])
    RCSL_BN_R = np.array([R6RC, R5RC, R4RC, R3RC, R2RC, R1RC, R0RC])
    np.set_printoptions(precision = 3, suppress = True)
#    font_tit = {'family': 'serif',
#        'color':  'darkred',
#        'weight': 'normal',
#        'size': 15,
#        }
#    font_ax = {'family': 'serif',
#        'color':  'black',
#        'weight': 'normal',
#        'size': 12,
#        }
#    plt.plot(RCSL_Z, RCSL_R, "co-")
#    plt.title("Return channel quasi-streamlines, meridional sketch", fontdict = font_tit)
#    plt.xlabel("Axial distance", fontdict = font_ax)
#    plt.ylabel("Radius", fontdict = font_ax)    
#    plt.xlim(-1.5*R2RC, 2.5*R2RC)
#    plt.ylim(0, 1.1*R5RC)
    return (np.around(RCSL_Z, decimals = 3), np.around(RCSL_R, decimals = 3), 
            np.around(RCSL_BN_Z, decimals = 3), np.around(RCSL_BN_R, decimals = 3))
    
    
    




    
    

    
