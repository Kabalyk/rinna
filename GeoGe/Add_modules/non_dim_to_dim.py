#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 18 18:11:29 2019

@author: kirill_DUO
"""
""" Rinna
    Copyright (C) 2019  Kirill Kabalyk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact e-mail: kirill.kabalyk@gmail.com
    
 """

""" The module contains functions to obtain real flowpath dimensions from dimensionless parameters """
 
import math
from numpy import asfortranarray as arr 
from numpy import around as around

# Function imp_sizes - returns real dimensions of an arbitraty centrigugal impeller (2D, 3D A-R, 3D mixed)
def imp_sizes(_D_im_out = 1.05, D2 = 500, _D_s_ec = 0.5, _D_h_ec = 0.4, _D_0 = 0.45, _D_h = 0.25, _b2 = 0.045, _b_s_ec = 0.06, _L_ax = 0.3, dtype = "float64"):
    Dia_vec = around(arr([_D_im_out*D2, D2, _D_s_ec*D2, _D_0*D2, _D_h*D2, _D_h_ec*D2]), decimals = 3)
    Dias = dict(D_im_out = Dia_vec[0], D2 = Dia_vec[1], D_s_ec = Dia_vec[2], D_0 = Dia_vec[3], D_h = Dia_vec[4], D_h_ec = Dia_vec[5])
    Radi_vec = Dia_vec/2
    Radii = dict(R_im_out = Radi_vec[0], R2 = Radi_vec[1], R_s_ec = Radi_vec[2], R_0 = Radi_vec[3], R_h = Radi_vec[4], R_h_ec = Radi_vec[5])
    b_vec = around(arr([_b2*D2, _b_s_ec*D2, _L_ax*D2]), decimals = 3)
    heights_a = dict(b2 = b_vec[0], b_s_ec = b_vec[1], L_ax = b_vec[2])
    imp_sizes.Dias = Dias
    imp_sizes.Radii = Radii
    imp_sizes.heights_a = heights_a
    return Dias, Radii, heights_a

# Function dif_sizes - returns real dimensions of a radial diffuser (vaneless, vaned)
def dif_sizes(_D_dif_in, D2, _D_s_ec_D, _D_h_ec_D, _D_4, _D_dif_out, _b_dif_in, _b_s_ec_D, _b_h_ec_D, dtype = "float64"):
    Dia_D_vec = around(arr([_D_dif_in*D2, D2, _D_s_ec_D*D2, _D_h_ec_D*D2, _D_4*D2, round(_D_dif_out*D2, 3)]), decimals = 3)
    Dias_D = dict(D_dif_in = Dia_D_vec[0], D_s_ec_D = Dia_D_vec[2], D_h_ec_D = Dia_D_vec[3], D_4 = Dia_D_vec[4], D_dif_out = Dia_D_vec[5])
    Radi_D_vec = Dia_D_vec/2
    Radii_D = dict(R_dif_in = Radi_D_vec[0], R_s_ec_D = Radi_D_vec[2], R_h_ec_D = Radi_D_vec[3], R_4 = Radi_D_vec[4], R_dif_out = Radi_D_vec[5])
    b_D_vec = around(arr([_b_dif_in*D2, _b_s_ec_D*D2, _b_h_ec_D*D2]), decimals = 3)
    heights_ax = dict(b_dif_in = b_D_vec[0], b_s_ec_D = b_D_vec[1], b_h_ec_D = b_D_vec[2])
    return Dias_D, Radii_D, heights_ax 
    
# Function RC_sizes - returns real dimensions of a return channel
def RC_sizes(_D_RC_in, D2, _D5, _D_s_RC_sc, _D_h_RC_sc, _D_s_7, _D_h_7, 
             b4, _b_s_RC_in, _b_h_RC_in, _b4_5, _b5, _dz_s_RC_sc, _dz_h_RC_sc, _L_RC_out, _Rc1, _Rc2, _dz_RC):
    Dia_RC_vec = around(arr([_D_RC_in*D2, D2, _D5*D2, _D_s_RC_sc*D2, _D_h_RC_sc*D2, _D_s_7*D2, _D_h_7*D2]), decimals = 3)
    Dias_RC = dict(D_RC_in = Dia_RC_vec[0], D5 = Dia_RC_vec[2], D_s_RC_sc = Dia_RC_vec[3], D_h_RC_sc = Dia_RC_vec[4], 
                   D_s_7 =  Dia_RC_vec[5], D_h_7 = Dia_RC_vec[6])
    Radi_RC_vec = Dia_RC_vec/2
    Radii_RC = dict(R_RC_in = Radi_RC_vec[0], R5 = Radi_RC_vec[2], R_s_RC_sc = Radi_RC_vec[3], R_h_RC_sc = Radi_RC_vec[4], 
                   R_s_7 =  Radi_RC_vec[5], R_h_7 = Radi_RC_vec[6], Rc1 = _Rc1*b4, Rc2 = _Rc2*_b4_5*b4)
    b_RC_vec = around(arr([_b_s_RC_in*b4, _b_h_RC_in*b4, b4, _b4_5*b4, _b5*b4, _dz_s_RC_sc*_b5*b4, _dz_h_RC_sc*_b5*b4, _dz_RC*_b4_5*b4, _L_RC_out*D2]),
                      decimals = 3)
    heights_RC_ax = dict(b_s_RC_in = b_RC_vec[0], b_h_RC_in = b_RC_vec[1], b4 = b_RC_vec[2],  b4_5 = b_RC_vec[3], b5 = b_RC_vec[4],
                         dz_s_RC_sc = b_RC_vec[5], dz_h_RC_sc = b_RC_vec[6], dz_RC = b_RC_vec[7], L_RC_out = b_RC_vec[8])
    return Dias_RC, Radii_RC, heights_RC_ax





    
    
    
    # return Radi_vec
    #return Dias["D_im_out"]
 