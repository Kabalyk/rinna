#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 21 19:21:58 2019

@author: kirill_DUO

"""

""" Rinna
    Copyright (C) 2019  Kirill Kabalyk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact e-mail: kirill.kabalyk@gmail.com
    
 """

""" The script computes and returns vectors of flowpath coordinates"""

import math
from matplotlib import pyplot as plt
from numpy import array as ar
from numpy import sin as sin
from numpy import cos as cos
from Add_modules import non_dim_to_dim as DIM
from Add_modules import flowpath_merid as MER
from Add_modules import QO_QS as QQ
from Add_modules import Radial_plane_profiling as Radd


#%% Closing previous windows

plt.close("all")

#%%  Name of the variant

Var_nm = "Flowpath_high_FI_manual_26_07_20.dat"

#%% Impeller input

_D_im_out = 1.05
D2 = 460 
_D_s_ec = 0.95
_D_h_ec = 0.95 
_D_0 = 0.50
_D_h = 0.25 
_b2 = 0.055
_b_s_ec = 0.056
_L_ax = 0.30
z_QSo = 5
k_Z_IQS = 0.5
k_R_IQS = 0.5
z_Bez_IQS = 20
z_int2D_IQS = 2


#%% Impeller domain meridional coords calcs (inlet section - blade - outlet section):

# Computing basic impeller sizes: 
I_s = DIM.imp_sizes(_D_im_out, D2, _D_s_ec, _D_h_ec, _D_0, _D_h, _b2, _b_s_ec, _L_ax)

# Computing impeller shroud curve:
kZ_IS = 0.5
kR_IS = 0.5
z_Bez_IS = 20
z_int2D_IS = 2
kZ3_IS = 0.5
Z3_IS = DIM.imp_sizes.heights_a["b_s_ec"] + kZ3_IS*(DIM.imp_sizes.Radii["R_s_ec"]-DIM.imp_sizes.Radii["R_0"])

I_Sh = MER.imp_strm_line(DIM.imp_sizes.heights_a["b2"], DIM.imp_sizes.heights_a["b2"], DIM.imp_sizes.heights_a["b_s_ec"], Z3_IS, 
                  DIM.imp_sizes.heights_a["L_ax"], DIM.imp_sizes.Radii["R_im_out"], DIM.imp_sizes.Radii["R2"], DIM.imp_sizes.Radii["R_s_ec"], 
                  DIM.imp_sizes.Radii["R_0"], DIM.imp_sizes.Radii["R_0"], kZ_IS, kR_IS, z_Bez_IS, z_int2D_IS)

# Computing impeller hub curve:
kZ_IH = 0.5
kR_IH = 0.5
z_Bez_IH = 20
z_int2D_IH = 2
kZ3_IH = 0.45
Z3_IH = kZ3_IH*(DIM.imp_sizes.Radii["R_h_ec"]-DIM.imp_sizes.Radii["R_h"])

I_H = MER.imp_strm_line(0, 0, 0, Z3_IH, DIM.imp_sizes.heights_a["L_ax"], DIM.imp_sizes.Radii["R_im_out"], DIM.imp_sizes.Radii["R2"], 
                        DIM.imp_sizes.Radii["R_h_ec"], DIM.imp_sizes.Radii["R_h"], DIM.imp_sizes.Radii["R_h"], kZ_IH, kR_IH, z_Bez_IH, z_int2D_IH)

# Compiling a vector of QO lengths:
QO_4_L = QQ.QO_length(I_Sh[2][0], I_H[2][0], I_Sh[3][0], I_H[3][0])
QO_3_L = QQ.QO_length(I_Sh[2][1], I_H[2][1], I_Sh[3][1], I_H[3][1])
QO_2_L = QQ.QO_length(I_Sh[2][2], I_H[2][2], I_Sh[3][2], I_H[3][2])
QO_1_L = QQ.QO_length(I_Sh[2][3], I_H[2][3], I_Sh[3][3], I_H[3][3])
QO_0_L = QQ.QO_length(I_Sh[2][4], I_H[2][4], I_Sh[3][4], I_H[3][4])

QO_L_V = ar([QO_4_L, QO_3_L, QO_2_L, QO_1_L, QO_0_L])

# Compiling a vector of QO inclinations:
QO_4_inc = QQ.QO_ax_inc(I_Sh[2][0], I_H[2][0], I_Sh[3][0], I_H[3][0])
QO_3_inc = QQ.QO_ax_inc(I_Sh[2][1], I_H[2][1], I_Sh[3][1], I_H[3][1])
QO_2_inc = QQ.QO_ax_inc(I_Sh[2][2], I_H[2][2], I_Sh[3][2], I_H[3][2])
QO_1_inc = QQ.QO_ax_inc(I_Sh[2][3], I_H[2][3], I_Sh[3][3], I_H[3][3])
QO_0_inc = QQ.QO_ax_inc(I_Sh[2][4], I_H[2][4], I_Sh[3][4], I_H[3][4])

QO_inc_V = ar([QO_4_inc, QO_3_inc, QO_2_inc, QO_1_inc, QO_0_inc])

# Computing intermediate QS nodes:
I_int_QSN = []
for j in range(2, z_QSo):
    I_int_QSN_j = QQ.Inter_QS_nodes(I_Sh[2], I_Sh[3], QO_L_V, QO_inc_V, z_QSo, j)
    I_int_QSN.append(I_int_QSN_j)

# Computing intermediate QS coords:
I_int_QSC = []
for jj in range(2, z_QSo):
    I_int_QSC_jj = MER.imp_strm_line(-I_int_QSN[jj-2][0][4], -I_int_QSN[jj-2][0][3], -I_int_QSN[jj-2][0][2], -I_int_QSN[jj-2][0][1], -I_int_QSN[jj-2][0][0],
                                     I_int_QSN[jj-2][1][4], I_int_QSN[jj-2][1][3], I_int_QSN[jj-2][1][2], I_int_QSN[jj-2][1][1], I_int_QSN[jj-2][1][0],
                                     k_R_IQS, k_Z_IQS, z_Bez_IQS, z_int2D_IQS)
    I_int_QSC.append(I_int_QSC_jj)
 
              
#%% Impeller domain meridional coords output:  
      
# Impeller domain basic sizes output:
print("Impeller basic dimensions: \n",
      "Dias: \n", I_s[0], "\n Radii: \n", I_s[1], "\n Axial sizes: \n", I_s[2], end = "\n\n")
with open(Var_nm, "w") as f:
    f.write("Impeller basic dimensions: \n" +
      "Dias: \n"+ str(I_s[0]) + "\n Radii: \n" + str(I_s[1]) + "\n Axial sizes: \n" + str(I_s[2]) + "\n\n")

# Impeller domain shroud coords output:  
print("Shrd line coords: \n",
      "Z_Shrd: \n", I_Sh[0], "\n R_Shrd: \n", I_Sh[1],
      "\n Shrd_line_nodes: \n", 
      "Z_Shrd_N: \n", I_Sh[2], "\n R_Shrd_N: \n", I_Sh[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Shrd line coords: \n" +
      "Z_Shrd: \n" + str(I_Sh[0]) + "\n R_Shrd: \n" + str(I_Sh[1]) +
      "\n Shrd_line_nodes: \n" + 
      "Z_Shrd_N: \n" + str(I_Sh[2]) + "\n R_Shrd_N: \n" + str(I_Sh[3]) + "\n\n")

# Impeller domain hub coords output:  
print("Hub line coords: \n",
      "Z_Hub: \n", I_H[0], "\n R_Hub: \n", I_H[1],
      "\n Hub_line_nodes: \n", 
      "Z_Hub_N: \n", I_H[2], "\n R_Hub_N: \n", I_H[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Hub line coords: \n" +
      "Z_Hub: \n" + str(I_H[0]) + "\n R_Hub: \n" + str(I_H[1]) +
      "\n Hub_line_nodes: \n" + 
      "Z_Hub_N: \n" + str(I_H[2]) + "\n R_Hub_N: \n" + str(I_H[3]) + "\n\n")    

# Impeller domain int QS nodes' coords output:  
for h in range(2, z_QSo):
    print("QS ", h, " nodes: \n"
          "Z_QS_", h,"_N:  \n", I_int_QSN[h-2][0],
          "\n R_QS_", h, "_N: \n", I_int_QSN[h-2][1], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("QS " + str(h) + " nodes: \n" +
          "Z_QS_" + str(h) +"_N:  \n" + str(I_int_QSN[h-2][0]) +
          "\n R_QS_"+ str(h) + "_N: \n" + str(I_int_QSN[h-2][1]) + "\n\n")    
        
# Impeller domain int QS coords output:          
for l in range(2, z_QSo):
    print("QS", l, "coords: \n",
          "Z_QS_", l, ": \n", I_int_QSC[l-2][0], "\n",
          "R_QS_", l, ": \n", I_int_QSC[l-2][1], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("QS" + str(l) + "coords: \n" +
          "Z_QS_" + str(l) + ": \n" + str(I_int_QSC[l-2][0]) + "\n" +
          "R_QS_" + str(l) + ": \n" + str(I_int_QSC[l-2][1]) + "\n\n")
        
        
#%% Impeller domain meridional profile plotting:
        
font_tit = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 15,
        }
plt.figure(figsize = (5, 5))
plt.plot(I_Sh[0], I_Sh[1], "bo-", markersize = 4)
plt.plot(I_H[0], I_H[1], "bo-", markersize = 4)
for hhk in range(2, z_QSo):
    plt.plot(I_int_QSC[hhk-2][0], I_int_QSC[hhk-2][1], "bo-", markersize = 4)
plt.title("Impeller quasi-streamlines, meridional sketch", fontdict = font_tit)
plt.xlabel("Axial distance", fontdict = font_tit)
plt.ylabel("Radius", fontdict = font_tit)

       
#%% Computing meridional lengths (dimensional and dimensionless) of impeller blade:
 
# LE n TE indices:
m_st = 1   
m_fin = 22

# Straight LE:

# Shroud line:    
I_Sh_ML = QQ.QS_mer_length(m_st, m_fin, I_Sh[0], I_Sh[1])

# Hub line:
I_H_ML = QQ.QS_mer_length(m_st, m_fin, I_H[0], I_H[1])
      
# int_QS lines:
I_int_QS_ML = []
for kk in range(0, z_QSo-2):
    I_int_QS_ML_i = QQ.QS_mer_length(m_st, m_fin, I_int_QSC[kk][0], I_int_QSC[kk][1])
    I_int_QS_ML.append(I_int_QS_ML_i)
    

#%% Impeller blade meridional length data output:

# Shroud line:
print("Shroud blade meridional data: \n",
      "Blade_Z_coords: \n", I_Sh_ML[0],  "\n Blade_R_coords: \n", I_Sh_ML[1],
      "\n Blade_mer_length_dim (excl. last pt): \n", I_Sh_ML[5], "\n Blade_mer_length_dimless (excl. last pt): \n", I_Sh_ML[6],
      "\n Blade total mer length: \n", I_Sh_ML[7], end = "\n\n")    
with open(Var_nm, "a") as f:
    f.write("Shroud blade meridional data: \n" +
      "Blade_Z_coords: \n" + str(I_Sh_ML[0]) +  "\n Blade_R_coords: \n" + str(I_Sh_ML[1]) +
      "\n Blade_mer_length_dim (excl. last pt): \n" + str(I_Sh_ML[5]) + "\n Blade_mer_length_dimless (excl. last pt): \n" + str(I_Sh_ML[6]) +
      "\n Blade total mer length: \n" + str(I_Sh_ML[7]) + "\n\n")

# Hub line:
print("Hub blade meridional data: \n",
      "Blade_Z_coords: \n", I_H_ML[0],  "\n Blade_R_coords: \n", I_H_ML[1],
      "\n Blade_mer_length_dim (excl. last pt): \n", I_H_ML[5], "\n Blade_mer_length_dimless (excl. last pt): \n", I_H_ML[6],
      "\n Blade total mer length: \n", I_H_ML[7], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Hub blade meridional data: \n" +
      "Blade_Z_coords: \n" + str(I_H_ML[0]) + "\n Blade_R_coords: \n" + str(I_H_ML[1]) +
      "\n Blade_mer_length_dim (excl. last pt): \n" + str(I_H_ML[5]) + "\n Blade_mer_length_dimless (excl. last pt): \n" + str(I_H_ML[6]) +
      "\n Blade total mer length: \n" + str(I_H_ML[7]) + "\n\n")

# int_QS lines:
for ll in range(2, z_QSo):
    print("QS", ll, "blade meridional data: \n",
          "Blade_Z_coords: \n", I_int_QS_ML[ll-2][0],  "\n Blade_R_coords: \n", I_int_QS_ML[ll-2][1],
           "\n Blade_mer_length_dim (excl. last pt): \n", I_int_QS_ML[ll-2][5], "\n Blade_mer_length_dimless (excl. last pt): \n", I_int_QS_ML[ll-2][6],
           "\n Blade total mer length: \n", I_int_QS_ML[ll-2][7], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("QS " + str(ll) + " blade meridional data: \n" +
          "Blade_Z_coords: \n" + str(I_int_QS_ML[ll-2][0]) +  "\n Blade_R_coords: \n" + str(I_int_QS_ML[ll-2][1]) +
           "\n Blade_mer_length_dim (excl. last pt): \n" + str(I_int_QS_ML[ll-2][5]) + "\n Blade_mer_length_dimless (excl. last pt): \n" + str(I_int_QS_ML[ll-2][6]) +
           "\n Blade total mer length: \n" + str(I_int_QS_ML[ll-2][7]) + "\n\n")


#%% Impeller radial coords calcs:
# Blade profile inlet:

beta_b1 = 28.0
beta_b2 = 65.0
A = 0.5
B = 0.5

# Blade profile at shroud:

I_BP_Sh = Radd.imp_rad_prof_LPI(beta_b1, beta_b2, D2/2, I_Sh_ML[1], A, B)

# Blade profile at hub:

I_BP_H = Radd.imp_rad_prof_LPI(beta_b1, beta_b2, D2/2, I_H_ML[1], A, B)

# Blade profile at int_QS lines:

I_BP_int_QS = []
for hh in range(0, z_QSo-2):
    I_BP_int_QS_i = Radd.imp_rad_prof_LPI(beta_b1, beta_b2, D2/2, I_int_QS_ML[hh][1], A, B)
    I_BP_int_QS.append(I_BP_int_QS_i)
        
      
#%%  Impeller blade radial (B-2-B) profile output (midline, blade angles):
        
# Blade profile at shroud:        
print("Shroud blade midline angles: \n", I_BP_Sh[1], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Shroud blade midline angles: \n" + str(I_BP_Sh[1]) + "\n\n")

# Blade profile at hub:
print("Hub blade midline angles: \n", I_BP_H[1], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Hub blade midline angles: \n" + str(I_BP_H[1]) + "\n\n")

# Blade profile at int_QS lines:      
for ss in range(2, z_QSo):
    print("QS", ss, "blade midline angles: \n", I_BP_int_QS[ss-2][1], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("QS" + str(ss) + "blade midline angles: \n" + str(I_BP_int_QS[ss-2][1]) + "\n\n")        

        
#%% Impeller blade angle Vs radius plotting (midline):

plt.figure(figsize = (5, 5))
plt.plot(I_Sh_ML[1], I_BP_Sh[1], "b^-", label = "Shroud", markersize = 4)
plt.plot(I_H_ML[1], I_BP_H[1], "r^-", label = "Hub", markersize = 4)
for ssj in range(2, z_QSo):
    plt.plot(I_int_QS_ML[ssj-2][1], I_BP_int_QS[ssj-2][1], "g^-", label = f"IQS nr {ssj}", markersize = 4)
plt.title("Impeller blade angles (midline)", fontdict = font_tit)
plt.xlabel("Radius", fontdict = font_tit)
plt.ylabel("Blade angle " + r"$\beta_{bl}$", fontdict = font_tit)
plt.legend()


#%% Computing impeller blade radial profile sketch: 
        
# Blade profile sketch at shroud:
I_BPS_Sh = Radd.rad_prof_sketch(I_Sh_ML[1], D2/2, I_BP_Sh[1], I_Sh_ML[4], 120)

# Blade profile sketch at hub:
I_BPS_H = Radd.rad_prof_sketch(I_H_ML[1], D2/2, I_BP_H[1], I_H_ML[4], 120)

# Blade profile sketch at int_QS lines:

I_BPS_int_QS = []
for xx in range(0, z_QSo-2):
    I_BPS_int_QS_i = Radd.rad_prof_sketch(I_int_QS_ML[xx][1], D2/2, I_BP_int_QS[xx][1], I_int_QS_ML[xx][4], 120)
    I_BPS_int_QS.append(I_BPS_int_QS_i)
        
        
#%% Impeller blade radial profile sketch output:

# Blade profile sketch at shroud:
print("Shroud blade midline sketch data: \n",
      "B2B plane lengths dl_rad: \n", I_BPS_Sh[0], "\n Central angle fi coords: \n", I_BPS_Sh[1],
      "\n X coords: \n", I_BPS_Sh[2], "\n Y coords: \n", I_BPS_Sh[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Shroud blade midline sketch data: \n" +
      "B2B plane lengths dl_rad: \n" + str(I_BPS_Sh[0]) + "\n Central angle fi coords: \n" + str(I_BPS_Sh[1]) +
      "\n X coords: \n" + str(I_BPS_Sh[2]) + "\n Y coords: \n" + str(I_BPS_Sh[3]) + "\n\n")

# Blade profile sketch at hub:
print("Hub blade midline sketch data: \n",
      "B2B plane lengths dl_rad: \n", I_BPS_H[0], "\n Central angle fi coords: \n", I_BPS_H[1],
      "\n X coords: \n", I_BPS_H[2], "\n Y coords: \n", I_BPS_H[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Hub blade midline sketch data: \n" +
      "B2B plane lengths dl_rad: \n" + str(I_BPS_H[0]) + "\n Central angle fi coords: \n" + str(I_BPS_H[1]) +
      "\n X coords: \n" + str(I_BPS_H[2]) + "\n Y coords: \n" + str(I_BPS_H[3]) + "\n\n")  

# Blade profile sketch at int_QS lines:
for ww in range(2, z_QSo):
    print("QS ", ww, " blade midline sketch data: \n",
          "B2B plane lengths dl_rad: \n", I_BPS_int_QS[ww-2][0], "\n Central angle fi coords: \n", I_BPS_int_QS[ww-2][1],
          "\n X coords: \n", I_BPS_int_QS[ww-2][2], "\n Y coords: \n", I_BPS_int_QS[ww-2][3], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("QS " + str(ww) + " blade midline sketch data: \n" +
          "B2B plane lengths dl_rad: \n" + str(I_BPS_int_QS[ww-2][0]) + "\n Central angle fi coords: \n" + str(I_BPS_int_QS[ww-2][1]) +
          "\n X coords: \n" + str(I_BPS_int_QS[ww-2][2]) + "\n Y coords: \n" + str(I_BPS_int_QS[ww-2][3]) + "\n\n")


#%% Impeller blade radial profile sketch plotting:

plt.figure(figsize = (5, 5))
#plt.plot(I_BPS_Sh[4]*cos(I_BPS_Sh[5]), I_BPS_Sh[4]*sin(I_BPS_Sh[5]), "g",
#         D2/2*cos(I_BPS_Sh[5]), D2/2*sin(I_BPS_Sh[5]), "g")
plt.plot(I_BPS_Sh[2], I_BPS_Sh[3], "bs", label = "Shroud", markersize = 4)
plt.plot(I_BPS_H[2], I_BPS_H[3], "rs", label = "Hub", markersize = 4)
for wwb in range(2, z_QSo):
    plt.plot(I_BPS_int_QS[wwb-2][2], I_BPS_int_QS[wwb-2][3], "gs", label = f"IQS nr {wwb}", markersize = 4)
plt.title("Impeller blade sketch (radial plane)", fontdict = font_tit)
plt.xlabel("X", fontdict = font_tit)
plt.ylabel("Y", fontdict = font_tit)
plt.xlim(right = D2/2)
plt.ylim(top = D2/2)
plt.legend()
      
        
 #%% Diffuser input:

_D_dif_in = _D_im_out
_D_s_ec_D = 1.1
_D_h_ec_D = 1.1 
_D_4 = 1.5
_D_dif_out = _D_4 * 1.05
_b_dif_in = _b2 
_b_s_ec_D = _b2 * 0.95
_b_h_ec_D = _b2 * 0.05
z_QSo = 5
k_Z_IQS = 0.5
k_R_IQS = 0.5
z_Bez_IQS = 10
z_int2D_IQS = 10

 
#%% Diffuser meridional coords calcs:
# Diffuser basic sizes:

D_s = DIM.dif_sizes(_D_dif_in, D2, _D_s_ec_D, _D_h_ec_D, _D_4, _D_dif_out, _b_dif_in, _b_s_ec_D, _b_h_ec_D)


# Computing diffuser shroud curve:
kZD_DS = 0.5
kRD_DS = 0.5
z_Bez_DS = 10
z_int2D_DS = 10

D_Sh = MER.dif_strm_line(D_s[2]["b_s_ec_D"], D_s[2]["b_s_ec_D"], D_s[2]["b_s_ec_D"], D_s[2]["b_dif_in"], 
                  D_s[1]["R_dif_out"], D_s[1]["R_4"], D_s[1]["R_s_ec_D"], D_s[1]["R_dif_in"], 
                  kZD_DS, kRD_DS, z_Bez_DS, z_int2D_DS)

# Computing diffuser hub curve:
kZD_DH = 0.5
kRD_DH = 0.5
z_Bez_DH = 10
z_int2D_DH = 10

D_H = MER.dif_strm_line(D_s[2]["b_h_ec_D"], D_s[2]["b_h_ec_D"], D_s[2]["b_h_ec_D"], 0, 
                  D_s[1]["R_dif_out"], D_s[1]["R_4"], D_s[1]["R_s_ec_D"], D_s[1]["R_dif_in"], 
                  kZD_DH, kRD_DH, z_Bez_DH, z_int2D_DH)

# Compiling a vector of diffuser QO lengths:
QO_3D_L = QQ.QO_length(D_Sh[2][0], D_H[2][0], D_Sh[3][0], D_H[3][0])
QO_2D_L = QQ.QO_length(D_Sh[2][1], D_H[2][1], D_Sh[3][1], D_H[3][1])
QO_1D_L = QQ.QO_length(D_Sh[2][2], D_H[2][2], D_Sh[3][2], D_H[3][2])
QO_0D_L = QQ.QO_length(D_Sh[2][3], D_H[2][3], D_Sh[3][3], D_H[3][3])

QO_D_L_V = ar([QO_3D_L, QO_2D_L, QO_1D_L, QO_0D_L])

# Compiling a vector of diffuser QO inclinations:
QO_3D_inc = QQ.QO_ax_inc(D_Sh[2][0], D_H[2][0], D_Sh[3][0], D_H[3][0])
QO_2D_inc = QQ.QO_ax_inc(D_Sh[2][1], D_H[2][1], D_Sh[3][1], D_H[3][1])
QO_1D_inc = QQ.QO_ax_inc(D_Sh[2][2], D_H[2][2], D_Sh[3][2], D_H[3][2])
QO_0D_inc = QQ.QO_ax_inc(D_Sh[2][3], D_H[2][3], D_Sh[3][3], D_H[3][3])

QO_D_inc_V = ar([QO_3D_inc, QO_2D_inc, QO_1D_inc, QO_0D_inc])       

# Computing diffuser intermediate QS nodes:
D_int_QSN = []
for u in range(2, z_QSo):
    D_int_QSN_u = QQ.Inter_QS_nodes(D_Sh[2], D_Sh[3], QO_D_L_V, QO_D_inc_V, z_QSo, u)
    D_int_QSN.append(D_int_QSN_u)
    
# Computing diffuser intermediate QS coords:
D_int_QSC = []
for jj in range(2, z_QSo):
    D_int_QSC_jj = MER.dif_strm_line(-D_int_QSN[jj-2][0][3], -D_int_QSN[jj-2][0][2], -D_int_QSN[jj-2][0][1], -D_int_QSN[jj-2][0][0], 
                                     D_int_QSN[jj-2][1][3], D_int_QSN[jj-2][1][2], D_int_QSN[jj-2][1][1], D_int_QSN[jj-2][1][0],
                                     k_R_IQS, k_Z_IQS, z_Bez_IQS, z_int2D_IQS)
    D_int_QSC.append(D_int_QSC_jj)
 
    
#%% Diffuser domain meridional profile output:
        
# Diffuser basic sizes:
print("_____________________________________________________________ \n",
      "Diffuser basic dimensions: \n",
      "Dias: \n", D_s[0], "\n Radii: \n", D_s[1], "\n Axial sizes: \n", D_s[2], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("_______________________________________________________ \n" +
            "Diffuser basic dimensions: \n" +
            "Dias: \n"+ str(D_s[0]) + "\n Radii: \n" + str(D_s[1]) + "\n Axial sizes: \n" + str(D_s[2]) + "\n\n")

# Diffuser shroud curve output:
print("Dif_Shrd line coords: \n",
      "Z_Shrd_D: \n", D_Sh[0], "\n R_Shrd_D: \n", D_Sh[1],
      "\n Diff_Shrd_line_nodes: \n", 
      "Z_Shrd_D_N: \n", D_Sh[2], "\n R_Shrd_D_N: \n", D_Sh[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Dif Shrd line coords: \n" +
      "Z_Shrd_D: \n" + str(D_Sh[0]) + "\n R_Shrd_D: \n" + str(D_Sh[1]) +
      "\n Diff_Shrd_line_nodes: \n" + 
      "Z_Shrd_D_N: \n" + str(D_Sh[2]) + "\n R_Shrd_D_N: \n" + str(D_Sh[3]) + "\n\n")

# Diffuser hub curve output:
print("Dif_Hub line coords: \n",
      "Z_Hub_D: \n", D_H[0], "\n R_Hub_D: \n", D_H[1],
      "\n Diff_Hub_line_nodes: \n", 
      "Z_Hub_D_N: \n", D_H[2], "\n R_Hub_D_N: \n", D_H[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Dif Hub line coords: \n" +
      "Z_Hub_D: \n" + str(D_H[0]) + "\n R_Hub_D: \n" + str(D_H[1]) +
      "\n Diff_Hub_line_nodes: \n" + 
      "Z_Hub_D_N: \n" + str(D_H[2]) + "\n R_Hub_D_N: \n" + str(D_H[3]) + "\n\n")   

# Diffuser intermediate QS nodes output:
for uu in range(2, z_QSo): 
    print("Diffuser QS ", uu, " nodes: \n"
          "Z_DQS_", uu,"_N:  \n", D_int_QSN[uu-2][0],
          "\n R_DQS_", uu, "_N: \n", D_int_QSN[uu-2][1], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("Difuser QS " + str(uu) + " nodes: \n" +
          "Z_DQS_" + str(uu) +"_N:  \n" + str(D_int_QSN[uu-2][0]) +
          "\n R_DQS_"+ str(uu) + "_N: \n" + str(D_int_QSN[uu-2][1]) + "\n\n")

# Diffuser intermediate QS coords output: 
for t in range(2, z_QSo):
    print("Diffuser QS", t, "coords: \n",
          "Z_DQS_", t, ": \n", D_int_QSC[t-2][0], "\n",
          "R_DQS_", t, ": \n", D_int_QSC[t-2][1], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("Diffuser QS" + str(t) + "coords: \n" +
          "Z_DQS_" + str(t) + ": \n" + str(D_int_QSC[t-2][0]) + "\n" +
          "R_QS_" + str(t) + ": \n" + str(D_int_QSC[t-2][1]) + "\n\n")

#%% Diffuser domain meridional profile plotting:

plt.figure(figsize = (5, 5))
plt.plot(D_Sh[0], D_Sh[1], "bo-", markersize = 4)
plt.plot(D_H[0], D_H[1], "bo-", markersize = 4)
for ttj in range(2, z_QSo):
    plt.plot(D_int_QSC[ttj-2][0], D_int_QSC[ttj-2][1], "bo-", markersize = 4)
plt.title("Radial diffuser meridional sketch", fontdict = font_tit)
plt.xlabel("Axial distance", fontdict = font_tit)
plt.ylabel("Radius", fontdict = font_tit)            


#%% Computing meridional lengths (dimensional and dimensionless) of QS, for VD:       
                        
# VD LE n TE indices:
i_st = 10
i_fin = 19

# Straight LE VD:
    # Shroud line VD:    
VD_Sh_ML = QQ.QS_mer_length(i_st, i_fin, D_Sh[0], D_Sh[1])
    
    # Hub line VD:
VD_H_ML = QQ.QS_mer_length(i_st, i_fin, D_H[0], D_H[1])
           
    # int_QS lines VD:
VD_int_QS_ML = []
for ss in range(0, z_QSo-2):
    VD_int_QS_ML_i = QQ.QS_mer_length(i_st, i_fin, D_int_QSC[ss][0], D_int_QSC[ss][1])
    VD_int_QS_ML.append(VD_int_QS_ML_i)
    
        
#%% Vaned diffuser meridional vane data output, for VD:

# Straight LE VD:
    # Shroud line VD:
print("Shroud VD meridional data: \n",
      "Vane_Z_coords: \n", VD_Sh_ML[0],  "\n Vane_R_coords: \n", VD_Sh_ML[1],
      "\n Vane_mer_length_dim (excl. last pt): \n", VD_Sh_ML[5], "\n Vane_mer_length_dimless (excl. last pt): \n", VD_Sh_ML[6],
      "\n Vane total mer length: \n", VD_Sh_ML[7], end = "\n\n")    
with open(Var_nm, "a") as f:
    f.write("Shroud VD meridional data: \n" +
      "Vane_Z_coords: \n" + str(VD_Sh_ML[0]) +  "\n Vane_R_coords: \n" + str(VD_Sh_ML[1]) +
      "\n Vane_mer_length_dim (excl. last pt): \n" + str(VD_Sh_ML[5]) + "\n Vane_mer_length_dimless (excl. last pt): \n" + str(VD_Sh_ML[6]) +
      "\n Vane total mer length: \n" + str(VD_Sh_ML[7]) + "\n\n")
    # Hub line VD:
print("Hub VD meridional data: \n",
      "Vane_Z_coords: \n", VD_H_ML[0],  "\n Blade_R_coords: \n", VD_H_ML[1],
      "\n Vane_mer_length_dim (excl. last pt): \n", VD_H_ML[5], "\n Vane_mer_length_dimless (excl. last pt): \n", VD_H_ML[6],
      "\n Vane total mer length: \n", VD_H_ML[7], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Hub VD meridional data: \n" +
      "Vane_Z_coords: \n" + str(VD_H_ML[0]) + "\n Vane_R_coords: \n" + str(VD_H_ML[1]) +
      "\n Vane_mer_length_dim (excl. last pt): \n" + str(VD_H_ML[5]) + "\n Vane_mer_length_dimless (excl. last pt): \n" + str(VD_H_ML[6]) +
      "\n Vane total mer length: \n" + str(VD_H_ML[7]) + "\n\n")
    # int_QS lines VD:
for ll in range(2, z_QSo):
    print("VD QS", ll, "vane meridional data: \n",
          "Vane_Z_coords: \n", VD_int_QS_ML[ll-2][0],  "\n Vane_R_coords: \n", VD_int_QS_ML[ll-2][1],
           "\n Vane_mer_length_dim (excl. last pt): \n", VD_int_QS_ML[ll-2][5], "\n Vane_mer_length_dimless (excl. last pt): \n", VD_int_QS_ML[ll-2][6],
           "\n Vane total mer length: \n", VD_int_QS_ML[ll-2][7], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("QS " + str(ll) + " vane meridional data: \n" +
          "Vane_Z_coords: \n" + str(VD_int_QS_ML[ll-2][0]) +  "\n Vane_R_coords: \n" + str(VD_int_QS_ML[ll-2][1]) +
           "\n Vane_mer_length_dim (excl. last pt): \n" + str(VD_int_QS_ML[ll-2][5]) + "\n Vane_mer_length_dimless (excl. last pt): \n" + str(VD_int_QS_ML[ll-2][6]) +
           "\n Vane total mer length: \n" + str(VD_int_QS_ML[ll-2][7]) + "\n\n")
        
         
#%% Vaned diffuser radial coords calcs:
        
# Vane profile input:

alpha_v3 = 29
alpha_v4 = 60
A = 0.5
B = 0.5

# Vane profile at shroud:

VD_VP_Sh = Radd.imp_rad_prof_LPI(alpha_v3, alpha_v4, _D_4*D2/2, VD_Sh_ML[1], A, B)

# Vane profile at hub:

VD_VP_H = Radd.imp_rad_prof_LPI(alpha_v3, alpha_v4, _D_4*D2/2, VD_H_ML[1], A, B)

# Vane profile at int_QS lines:

VD_VP_int_QS = []
for aaa in range(0, z_QSo-2):
    VD_VP_int_QS_i = Radd.imp_rad_prof_LPI(alpha_v3, alpha_v4, _D_4*D2/2, VD_int_QS_ML[aaa][1], A, B)
    VD_VP_int_QS.append(VD_VP_int_QS_i)


#%% Vaned diffuser radial coords output:

# Vane profile at shroud:
print("Shroud VD vane midline angles: \n", VD_VP_Sh[1], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Shroud VD vane midline angles: \n" + str(VD_VP_Sh[1]) + "\n\n")
    
# Vane profile at hub:
print("Hub VD vane midline angles: \n", VD_VP_H[1], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Hub VD vane midline angles: \n" + str(VD_VP_H[1]) + "\n\n")

# Vane profile at int_QS lines:
for bbb in range(2, z_QSo):
    print("VD QS", bbb, "vane midline angles: \n", VD_VP_int_QS[bbb-2][1], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("VD QS" + str(bbb) + "vane midline angles: \n" + str(VD_VP_int_QS[bbb-2][1]) + "\n\n")


#%% Vaned diffuser angle Vs radius plotting:
        
plt.figure(figsize = (5, 5))
plt.plot(VD_Sh_ML[1], VD_VP_Sh[1], "b^-", label = "Shroud", markersize = 4)
plt.plot(VD_H_ML[1], VD_VP_H[1], "r^-", label = "Hub", markersize = 4)
for bbw in range(2, z_QSo):
    plt.plot(VD_int_QS_ML[bbw-2][1], VD_VP_int_QS[bbw-2][1], "g^-", label = f"IQS nr {bbw}", markersize = 4)
plt.title("VD vane angles (midline)", fontdict = font_tit)
plt.xlabel("Radius", fontdict = font_tit)
plt.ylabel("Vane angle " + r"$\alpha_{v}$", fontdict = font_tit)
plt.legend()


#%%  Computing VD vane profile sketch:      
# Vane profile sketch at shroud:

VD_VPS_Sh = Radd.rad_prof_sketch(VD_Sh_ML[1], _D_4*D2/2, VD_VP_Sh[1], VD_Sh_ML[4], 120)

# Vane profile sketch at hub:

VD_VPS_H = Radd.rad_prof_sketch(VD_H_ML[1], _D_4*D2/2, VD_VP_H[1], VD_H_ML[4], 120)

# VD profile sketch at int_QS lines:
VD_VPS_int_QS = []
for xxx in range(0, z_QSo-2):
    VD_VPS_int_QS_i = Radd.rad_prof_sketch(VD_int_QS_ML[xxx][1], _D_4*D2/2, VD_VP_int_QS[xxx][1], VD_int_QS_ML[xxx][4], 120) # bug here, correct!
    VD_VPS_int_QS.append(VD_VPS_int_QS_i)
            

#%% VD vane profile sketch data output:

# Vane profile sketch at shroud:
print("Shroud VD vane midline sketch data: \n",
      "B2B plane lengths dl_rad: \n", VD_VPS_Sh[0], "\n Central angle fi coords: \n", VD_VPS_Sh[1],
      "\n X coords: \n", VD_VPS_Sh[2], "\n Y coords: \n", VD_VPS_Sh[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Shroud VD vane midline sketch data: \n" +
      "B2B plane lengths dl_rad: \n" + str(VD_VPS_Sh[0]) + "\n Central angle fi coords: \n" + str(VD_VPS_Sh[1]) +
      "\n X coords: \n" + str(VD_VPS_Sh[2]) + "\n Y coords: \n" + str(VD_VPS_Sh[3]) + "\n\n")

# Vane profile sketch at hub:
print("Hub VD vane midline sketch data: \n",
      "B2B plane lengths dl_rad: \n", VD_VPS_H[0], "\n Central angle fi coords: \n", VD_VPS_H[1],
      "\n X coords: \n", VD_VPS_H[2], "\n Y coords: \n", VD_VPS_H[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Hub VD vane midline sketch data: \n" +
      "B2B plane lengths dl_rad: \n" + str(VD_VPS_H[0]) + "\n Central angle fi coords: \n" + str(VD_VPS_H[1]) +
      "\n X coords: \n" + str(VD_VPS_H[2]) + "\n Y coords: \n" + str(VD_VPS_H[3]) + "\n\n")

# VD profile sketch at int_QS lines:
for www in range(2, z_QSo):
    print("VD QS ", www, " vane midline sketch data: \n",
          "B2B plane lengths dl_rad: \n", VD_VPS_int_QS[www-2][0], "\n Central angle fi coords: \n", VD_VPS_int_QS[www-2][1],
          "\n X coords: \n", VD_VPS_int_QS[www-2][2], "\n Y coords: \n", VD_VPS_int_QS[www-2][3], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("QS VD" + str(www) + " vane midline sketch data: \n" +
          "B2B plane lengths dl_rad: \n" + str(VD_VPS_int_QS[www-2][0]) + "\n Central angle fi coords: \n" + str(VD_VPS_int_QS[www-2][1]) +
          "\n X coords: \n" + str(VD_VPS_int_QS[www-2][2]) + "\n Y coords: \n" + str(VD_VPS_int_QS[www-2][3]) + "\n\n")       
        

#%% VD vane profile sketch plotting:

plt.figure(figsize = (5, 5))
plt.plot(VD_VPS_Sh[2], VD_VPS_Sh[3], "bs", label = "Shroud", markersize = 4)
plt.plot(VD_VPS_H[2], VD_VPS_H[3], "rs", label = "Hub", markersize = 4)
for wwv in range (2, z_QSo):
    plt.plot(VD_VPS_int_QS[wwv-2][2], VD_VPS_int_QS[wwv-2][3], "gs", label = f"IQS nr {wwv}", markersize = 4)
plt.title("VD vane profile sketch", fontdict = font_tit)
plt.xlabel("X", fontdict = font_tit)
plt.ylabel("Y", fontdict = font_tit)
plt.legend()
        
    
 #%%   Return channel input:

_D_RC_in = _D_dif_out
_D5 = _D_RC_in * 1
_D_s_RC_sc = 0.55
_D_h_RC_sc = 0.53
_D_s_7 = 0.45
_D_h_7 = 0.25
b4 =  round(abs(D_Sh[0][-1] - D_H[0][-1]), 1)
_b_s_RC_in = -D_Sh[0][-1]/b4 
_b_h_RC_in = -D_H[0][-1]/b4  
_b4_5 = 1   
_b5 = 1
_dz_s_RC_sc = 0.1
_dz_h_RC_sc = 0
_L_RC_out = 0.5
_Rc1 = 1
_Rc2 = 1
_dz_RC = 0.1


#%% RC meridional coords calcs:
# RC basic sizes:

RC_s = DIM.RC_sizes(_D_RC_in, D2, _D5, _D_s_RC_sc, _D_h_RC_sc, _D_s_7, _D_h_7,
                    b4, _b_s_RC_in, _b_h_RC_in, _b4_5, _b5, _dz_s_RC_sc, _dz_h_RC_sc, _L_RC_out, _Rc1, _Rc2, _dz_RC)

# Computing RC shroud curve:
    
kZRC12_RCS = 0.5
kRRC12_RCS = 0.5
kZRC34_RCS = 0.5
kRRC34_RCS = 0.5
kZRC56_RCS = 0.5
kRRC56_RCS = 0.5
zRC_Bez_RCS = 10
zRC_int2D_RCS = 10
kZ1_RCS = 0.85
Z0_RCS = RC_s[2]["L_RC_out"]
Z1_RCS = (-RC_s[2]["b_s_RC_in"] + b4 + RC_s[1]["Rc1"] +
          RC_s[2]["dz_RC"] + RC_s[1]["Rc2"] + RC_s[2]["b5"] +
          RC_s[2]["dz_s_RC_sc"]) + kZ1_RCS*(RC_s[1]["R_s_RC_sc"] - RC_s[1]["R_s_7"])
Z2_RCS = -RC_s[2]["b_s_RC_in"] + b4 + RC_s[1]["Rc1"] + RC_s[2]["dz_RC"] + RC_s[1]["Rc2"] + RC_s[2]["b5"] + RC_s[2]["dz_s_RC_sc"]
Z3_RCS = -RC_s[2]["b_s_RC_in"] + b4 + RC_s[1]["Rc1"] + RC_s[2]["dz_RC"] + RC_s[1]["Rc2"] + RC_s[2]["b5"]
Z4_RCS = -RC_s[2]["b_s_RC_in"] + b4 + RC_s[1]["Rc1"] + RC_s[2]["dz_RC"]
Z5_RCS = -RC_s[2]["b_s_RC_in"] + b4 + RC_s[1]["Rc1"]
Z6_RCS = -RC_s[2]["b_s_RC_in"]
R0_RCS = RC_s[1]["R_s_7"]
R1_RCS = RC_s[1]["R_s_7"]
R2_RCS = RC_s[1]["R_s_RC_sc"]
R3_RCS = RC_s[1]["R5"]
R4_RCS = RC_s[1]["R_RC_in"] + RC_s[1]["Rc2"] + RC_s[2]["b4_5"]
R5_RCS = RC_s[1]["R_RC_in"] + RC_s[1]["Rc1"] + RC_s[2]["b4_5"]
R6_RCS = RC_s[1]["R_RC_in"]

RC_Sh = MER.RC_strm_line(Z0_RCS, Z1_RCS, Z2_RCS, Z3_RCS, Z4_RCS, Z5_RCS, Z6_RCS,
                         R0_RCS, R1_RCS, R2_RCS, R3_RCS, R4_RCS, R5_RCS, R6_RCS,
                         kRRC12_RCS, kZRC12_RCS, kRRC34_RCS, kZRC34_RCS, kRRC56_RCS, kZRC56_RCS,
                         zRC_Bez_RCS, zRC_int2D_RCS)

# Computing RC hub curve:
    
kZRC12_RCH = 0.5
kRRC12_RCH = 0.5
kZRC34_RCH = 0.5
kRRC34_RCH = 0.5
kZRC56_RCH = 0.5
kRRC56_RCH = 0.5
zRC_Bez_RCH = 10
zRC_int2D_RCH = 10
kZ1_RCH = 0.55
Z0_RCH = RC_s[2]["L_RC_out"]
Z1_RCH = (-RC_s[2]["b_h_RC_in"] + RC_s[1]["Rc1"] + RC_s[2]["dz_RC"] + 
          RC_s[1]["Rc2"] + RC_s[2]["dz_h_RC_sc"]) + kZ1_RCH*(RC_s[1]["R_h_RC_sc"] - RC_s[1]["R_h_7"])
Z2_RCH = (-RC_s[2]["b_h_RC_in"] + RC_s[1]["Rc1"] + RC_s[2]["dz_RC"] + 
          RC_s[1]["Rc2"] + RC_s[2]["dz_h_RC_sc"])
Z3_RCH = (-RC_s[2]["b_h_RC_in"] + RC_s[1]["Rc1"] + RC_s[2]["dz_RC"] + 
          RC_s[1]["Rc2"])
Z4_RCH = (-RC_s[2]["b_h_RC_in"] + RC_s[1]["Rc1"] + RC_s[2]["dz_RC"])
Z5_RCH = (-RC_s[2]["b_h_RC_in"] + RC_s[1]["Rc1"])
Z6_RCH = -RC_s[2]["b_h_RC_in"]
R0_RCH = RC_s[1]["R_h_7"]
R1_RCH = RC_s[1]["R_h_7"]
R2_RCH = RC_s[1]["R_h_RC_sc"]
R3_RCH = RC_s[1]["R5"]
R4_RCH = RC_s[1]["R_RC_in"] + RC_s[1]["Rc2"]
R5_RCH = RC_s[1]["R_RC_in"] + RC_s[1]["Rc1"]
R6_RCH = RC_s[1]["R_RC_in"]

RC_H = MER.RC_strm_line(Z0_RCH, Z1_RCH, Z2_RCH, Z3_RCH, Z4_RCH, Z5_RCH, Z6_RCH,
                        R0_RCH, R1_RCH, R2_RCH, R3_RCH, R4_RCH, R5_RCH, R6_RCH,
                        kRRC12_RCH, kZRC12_RCH, kRRC34_RCH, kZRC34_RCH, kRRC56_RCH, kZRC56_RCH,
                        zRC_Bez_RCH, zRC_int2D_RCH)

# Compiling a vector of return-channel QO lengths:

QO_6RC_L = QQ.QO_length(RC_Sh[2][0], RC_H[2][0], RC_Sh[3][0], RC_H[3][0])
QO_5RC_L = QQ.QO_length(RC_Sh[2][1], RC_H[2][1], RC_Sh[3][1], RC_H[3][1])    
QO_4RC_L = QQ.QO_length(RC_Sh[2][2], RC_H[2][2], RC_Sh[3][2], RC_H[3][2])    
QO_3RC_L = QQ.QO_length(RC_Sh[2][3], RC_H[2][3], RC_Sh[3][3], RC_H[3][3])
QO_2RC_L = QQ.QO_length(RC_Sh[2][4], RC_H[2][4], RC_Sh[3][4], RC_H[3][4])    
QO_1RC_L = QQ.QO_length(RC_Sh[2][5], RC_H[2][5], RC_Sh[3][5], RC_H[3][5]) 
QO_0RC_L = QQ.QO_length(RC_Sh[2][6], RC_H[2][6], RC_Sh[3][6], RC_H[3][6]) 

QO_RC_L_V = ar([QO_6RC_L, QO_5RC_L, QO_4RC_L, QO_3RC_L, QO_2RC_L, QO_1RC_L, QO_0RC_L])
    
# Compiling a vector of return-channel QO inclinations:   

QO_6RC_inc = QQ.QO_ax_inc(RC_Sh[2][0], RC_H[2][0], RC_Sh[3][0], RC_H[3][0]) 
QO_5RC_inc = QQ.QO_ax_inc(RC_Sh[2][1], RC_H[2][1], RC_Sh[3][1], RC_H[3][1])
QO_4RC_inc = QQ.QO_ax_inc(RC_Sh[2][2], RC_H[2][2], RC_Sh[3][2], RC_H[3][2])
QO_3RC_inc = QQ.QO_ax_inc(RC_Sh[2][3], RC_H[2][3], RC_Sh[3][3], RC_H[3][3])
QO_2RC_inc = QQ.QO_ax_inc(RC_Sh[2][4], RC_H[2][4], RC_Sh[3][4], RC_H[3][4])
QO_1RC_inc = QQ.QO_ax_inc(RC_Sh[2][5], RC_H[2][5], RC_Sh[3][5], RC_H[3][5])
QO_0RC_inc = QQ.QO_ax_inc(RC_Sh[2][6], RC_H[2][6], RC_Sh[3][6], RC_H[3][6])

QO_RC_inc_V = ar([QO_6RC_inc, QO_5RC_inc, QO_4RC_inc, QO_3RC_inc, QO_2RC_inc, QO_1RC_inc, QO_0RC_inc])

# Computing RC intermediate QS nodes:

RC_int_QSN = []
for p in range(2, z_QSo):
    RC_int_QSN_p = QQ.Inter_QS_nodes(RC_Sh[2], RC_Sh[3], QO_RC_L_V, QO_RC_inc_V, z_QSo, p) 
    RC_int_QSN.append(RC_int_QSN_p)
   
# Computing RC intermediate QS coords:
        
kZRC12_RC_IQS = 0.5
kRRC12_RC_IQS = 0.5
kZRC34_RC_IQS = 0.5
kRRC34_RC_IQS = 0.5
kZRC56_RC_IQS = 0.5
kRRC56_RC_IQS = 0.5
zRC_Bez_RC_IQS = 10
zRC_int2D_RC_IQS = 10

RC_int_QSC = []
for yy in range(2, z_QSo):
    RC_int_QSC_yy = MER.RC_strm_line(RC_int_QSN[yy-2][0][6], RC_int_QSN[yy-2][0][5], RC_int_QSN[yy-2][0][4], RC_int_QSN[yy-2][0][3], RC_int_QSN[yy-2][0][2], 
                                     RC_int_QSN[yy-2][0][1], RC_int_QSN[yy-2][0][0],RC_int_QSN[yy-2][1][6], RC_int_QSN[yy-2][1][5], RC_int_QSN[yy-2][1][4],
                                     RC_int_QSN[yy-2][1][3], RC_int_QSN[yy-2][1][2], RC_int_QSN[yy-2][1][1], RC_int_QSN[yy-2][1][0],
                                     kRRC12_RC_IQS, kZRC12_RC_IQS, kRRC34_RC_IQS, kZRC34_RC_IQS, kRRC56_RC_IQS, kZRC56_RC_IQS, 
                                     zRC_Bez_RC_IQS, zRC_int2D_RC_IQS) 
    RC_int_QSC.append(RC_int_QSC_yy)
    
                  
#%% RC domain meridional coordinates output:

# RC basic sizes:
print("_____________________________________________________________ \n",
      "RC basic dimensions: \n",
      "Dias: \n", RC_s[0], "\n Radii: \n", RC_s[1], "\n Axial sizes: \n", RC_s[2], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("_______________________________________________________ \n" +
            "RC basic dimensions: \n" +
            "Dias: \n"+ str(RC_s[0]) + "\n Radii: \n" + str(RC_s[1]) + "\n Axial sizes: \n" + str(RC_s[2]) + "\n\n")

# RC shroud curve output:
print("RC_Shrd line coords: \n",
      "Z_Shrd_RC: \n", RC_Sh[0], "\n R_Shrd_RC: \n", RC_Sh[1],
      "\n RC_Shrd_line_nodes: \n", 
      "Z_Shrd_RC_N: \n", RC_Sh[2], "\n R_Shrd_RC_N: \n", RC_Sh[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("RC Shrd line coords: \n" +
      "Z_Shrd_RC: \n" + str(RC_Sh[0]) + "\n R_Shrd_RC: \n" + str(RC_Sh[1]) +
      "\n Diff_Shrd_line_nodes: \n" + 
      "Z_Shrd_RC_N: \n" + str(RC_Sh[2]) + "\n R_Shrd_RC_N: \n" + str(RC_Sh[3]) + "\n\n")

# RC hub curve output:
print("RC_Hub line coords: \n",
      "Z_Hub_RC: \n", RC_H[0], "\n R_Hub_RC: \n", RC_H[1],
      "\n RC_Hub_line_nodes: \n", 
      "Z_Hub_RC_N: \n", RC_H[2], "\n R_Hub_RC_N: \n", RC_H[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("RC Hub line coords: \n" +
      "Z_Hub_RC: \n" + str(RC_H[0]) + "\n R_Hub_RC: \n" + str(RC_H[1]) +
      "\n Diff_Hub_line_nodes: \n" + 
      "Z_Hub_RC_N: \n" + str(RC_H[2]) + "\n R_Hub_RC_N: \n" + str(RC_H[3]) + "\n\n")

# RC intermediate QS nodes output:
for pp in range(2, z_QSo): 
    print("Return channel QS ", pp, " nodes: \n"
          "Z_RCQS_", pp,"_N:  \n", RC_int_QSN[pp-2][0],
          "\n R_RCQS_", pp, "_N: \n", RC_int_QSN[pp-2][1], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("Return channel QS " + str(pp) + " nodes: \n" +
          "Z_RCQS_" + str(pp) +"_N:  \n" + str(RC_int_QSN[pp-2][0]) +
          "\n R_RCQS_"+ str(pp) + "_N: \n" + str(RC_int_QSN[pp-2][1]) + "\n\n")
        
# RC intermediate QS coords output:
for tx in range(2, z_QSo):
    print("RC QS", tx, "coords: \n",
          "Z_RCQS_", tx, ": \n", RC_int_QSC[tx-2][0], "\n",
          "R_RCQS_", tx, ": \n", RC_int_QSC[tx-2][1], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("RC QS" + str(tx) + "coords: \n" +
          "Z_RCQS_" + str(tx) + ": \n" + str(RC_int_QSC[tx-2][0]) + "\n" +
          "R_RCQS_" + str(tx) + ": \n" + str(RC_int_QSC[tx-2][1]) + "\n\n")  


#%% RC domain meridional plot:

plt.figure(figsize = (5, 5))
plt.plot(RC_Sh[0], RC_Sh[1], "bo-", markersize = 4)
plt.plot(RC_H[0], RC_H[1], "bo-", markersize = 4)
for tte in range(2, z_QSo):
    plt.plot(RC_int_QSC[tte-2][0], RC_int_QSC[tte-2][1], "bo-", markersize = 4)
plt.title("RC domain meridional sketch", fontdict = font_tit )
plt.xlabel("Axial distance", fontdict = font_tit)
plt.ylabel("Radius", fontdict = font_tit)            
     
        
#%% Computing meridional lengths (dimensional and dimensionless) of QS, for RC:

# RC LE n TE indices:
k_st = 19
k_fin = 29

# Straight LE RC:
    # Shroud line RC:    
RC_Sh_ML = QQ.QS_mer_length(k_st, k_fin, RC_Sh[0], RC_Sh[1])

    # Hub line RC:
RC_H_ML = QQ.QS_mer_length(k_st, k_fin, RC_H[0], RC_H[1])
    
    # int_QS lines RC:
RC_int_QS_ML = []
for ssa in range(2, z_QSo):
    RC_int_QS_ML_ssa = QQ.QS_mer_length(k_st, k_fin, RC_int_QSC[ssa-2][0], RC_int_QSC[ssa-2][1])
    RC_int_QS_ML.append(RC_int_QS_ML_ssa)  


#%% RC meridional vane data output:
        
# Straight LE RC:
    # Shroud line RC output:  
print("Shroud RC meridional data: \n",
      "RC_Vane_Z_coords: \n", RC_Sh_ML[0],  "\n RC_Vane_R_coords: \n", RC_Sh_ML[1],
      "\n RC_Vane_mer_length_dim (excl. last pt): \n", RC_Sh_ML[5], "\n RC_Vane_mer_length_dimless (excl. last pt): \n", RC_Sh_ML[6],
      "\n RC_Vane total mer length: \n", RC_Sh_ML[7], end = "\n\n")    
with open(Var_nm, "a") as f:
    f.write("Shroud RC meridional data: \n" +
      "RC_Vane_Z_coords: \n" + str(RC_Sh_ML[0]) +  "\n RC_Vane_R_coords: \n" + str(RC_Sh_ML[1]) +
      "\n RC_Vane_mer_length_dim (excl. last pt): \n" + str(RC_Sh_ML[5]) + "\n RC_Vane_mer_length_dimless (excl. last pt): \n" + str(RC_Sh_ML[6]) +
      "\n RC_Vane total mer length: \n" + str(RC_Sh_ML[7]) + "\n\n")

    # Hub line RC output:     
print("Hub RC meridional data: \n",
      "RC_Vane_Z_coords: \n", RC_H_ML[0],  "\n RC_Vane_R_coords: \n", RC_H_ML[1],
      "\n RC_Vane_mer_length_dim (excl. last pt): \n", RC_H_ML[5], "\n RC_Vane_mer_length_dimless (excl. last pt): \n", RC_H_ML[6],
      "\n RC_Vane total mer length: \n", RC_H_ML[7], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Hub RC meridional data: \n" +
      "RC_Vane_Z_coords: \n" + str(RC_H_ML[0]) + "\n RC_Vane_R_coords: \n" + str(RC_H_ML[1]) +
      "\n RC_Vane_mer_length_dim (excl. last pt): \n" + str(RC_H_ML[5]) + "\n RC_Vane_mer_length_dimless (excl. last pt): \n" + str(RC_H_ML[6]) +
      "\n RC_Vane total mer length: \n" + str(RC_H_ML[7]) + "\n\n")
    
    # int_QS lines RC output:
for lla in range(2, z_QSo):
    print("RC QS", lla, "vane meridional data: \n",
          "RC_Vane_Z_coords: \n", RC_int_QS_ML[lla-2][0],  "\n RC_Vane_R_coords: \n", RC_int_QS_ML[lla-2][1],
           "\n RC_Vane_mer_length_dim (excl. last pt): \n", RC_int_QS_ML[lla-2][5], "\n RC_Vane_mer_length_dimless (excl. last pt): \n", RC_int_QS_ML[lla-2][6],
           "\n RC Vane total mer length: \n", RC_int_QS_ML[lla-2][7], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("RC_QS " + str(lla) + " vane meridional data: \n" +
          "RC_Vane_Z_coords: \n" + str(RC_int_QS_ML[lla-2][0]) +  "\n RC_Vane_R_coords: \n" + str(RC_int_QS_ML[lla-2][1]) +
           "\n RC_Vane_mer_length_dim (excl. last pt): \n" + str(RC_int_QS_ML[lla-2][5]) + "\n RC_Vane_mer_length_dimless (excl. last pt): \n" + str(RC_int_QS_ML[lla-2][6]) +
           "\n RC Vane total mer length: \n" + str(RC_int_QS_ML[lla-2][7]) + "\n\n")
        
        
#%% Return channel radial coords calcs:
# RC Vane profile input:

alpha_v5 = 50
alpha_v6 = 90
_D6 = 0.5 * (_D_s_RC_sc + _D_h_RC_sc) 
A = 0.5
B = 0.5
        
# RC Vane profile at shroud:
RC_VP_Sh = Radd.imp_rad_prof_LPI(alpha_v5, alpha_v6, _D6*D2/2, RC_Sh_ML[1], A, B)

# RC Vane profile at hub:
RC_VP_H = Radd.imp_rad_prof_LPI(alpha_v5, alpha_v6, _D6*D2/2, RC_H_ML[1], A, B)

# RC Vane profile at int_QS lines:
RC_VP_int_QS = []
for qwe in range(0, z_QSo-2):
    RC_VP_int_QS_qwe = Radd.imp_rad_prof_LPI(alpha_v5, alpha_v6, _D6*D2/2, RC_int_QS_ML[qwe][1], A, B)
    RC_VP_int_QS.append(RC_VP_int_QS_qwe)

       
#%% Return channel vane profile output:
        
# RC Vane profile at shroud output:
print("Shroud RC vane midline angles: \n", RC_VP_Sh[1], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Shroud RC vane midline angles: \n" + str(RC_VP_Sh[1]) + "\n\n") 
    
# RC vane profile at hub output:
print("Hub RC vane midline angles: \n", RC_VP_H[1], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Hub RC vane midline angles: \n" + str(RC_VP_H[1]) + "\n\n")
    
# RC Vane profile at int_QS lines output:    
for rty in range(2, z_QSo):
    print("RC QS", rty, "vane midline angles: \n", RC_VP_int_QS[rty-2][1], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("RC QS" + str(rty) + "vane midline angles: \n" + str(RC_VP_int_QS[rty-2][1]) + "\n\n")   
            
    
#%% Return channel vane profile plotting:

plt.figure(figsize = (5, 5))
plt.plot(RC_Sh_ML[1], RC_VP_Sh[1], "b^-", label = "Shroud", markersize = 4)
plt.plot(RC_H_ML[1], RC_VP_H[1], "r^-", label = "Hub", markersize = 4)
for ytr in range(2, z_QSo):
    plt.plot(RC_int_QS_ML[ytr-2][1], RC_VP_int_QS[ytr-2][1], "g^-", 
             label = f"IQS nr {ytr}", markersize = 4)
plt.title("RC vane angles (midline)", fontdict = font_tit)        
plt.xlabel("Radius", fontdict = font_tit)
plt.ylabel("Vane angle " + r"$\alpha_{v}$", fontdict = font_tit)
plt.legend()


#%% Computing RC vane profile sketch:
          
# RC Vane profile sketch at shroud:
RC_VPS_Sh = Radd.rad_prof_sketch(RC_Sh_ML[1], _D6*D2/2, RC_VP_Sh[1], RC_Sh_ML[4], 120)

# RC Vane profile sketch at hub:
RC_VPS_H = Radd.rad_prof_sketch(RC_H_ML[1], _D6*D2/2, RC_VP_H[1], RC_H_ML[4], 120)

# RC profile sketch at int_QS lines:
RC_VPS_int_QS = []
for ttr in range(0, z_QSo-2):
    RC_VPS_int_QS_ttr = Radd.rad_prof_sketch(RC_int_QS_ML[ttr][1], _D6*D2/2, RC_VP_int_QS[ttr][1], RC_int_QS_ML[ttr][4], 120) # bug here, correct!
    RC_VPS_int_QS.append(RC_VPS_int_QS_ttr)


#%% RC vane profile sketch output:
    
# RC Vane profile sketch at shroud output: 
print("Shroud RC vane midline sketch data: \n",
      "B2B plane lengths dl_rad: \n", RC_VPS_Sh[0], "\n Central angle fi coords: \n", RC_VPS_Sh[1],
      "\n X coords: \n", RC_VPS_Sh[2], "\n Y coords: \n", RC_VPS_Sh[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Shroud RC vane midline sketch data: \n" +
      "B2B plane lengths dl_rad: \n" + str(RC_VPS_Sh[0]) + "\n Central angle fi coords: \n" + str(RC_VPS_Sh[1]) +
      "\n X coords: \n" + str(RC_VPS_Sh[2]) + "\n Y coords: \n" + str(RC_VPS_Sh[3]) + "\n\n")    
    
# RC Vane profile sketch at hub output:
print("Hub RC vane midline sketch data: \n",
      "B2B plane lengths dl_rad: \n", RC_VPS_H[0], "\n Central angle fi coords: \n", RC_VPS_H[1],
      "\n X coords: \n", RC_VPS_H[2], "\n Y coords: \n", RC_VPS_H[3], end = "\n\n")
with open(Var_nm, "a") as f:
    f.write("Hub RC vane midline sketch data: \n" +
      "B2B plane lengths dl_rad: \n" + str(RC_VPS_H[0]) + "\n Central angle fi coords: \n" + str(RC_VPS_H[1]) +
      "\n X coords: \n" + str(RC_VPS_H[2]) + "\n Y coords: \n" + str(RC_VPS_H[3]) + "\n\n")    
    
# RC profile sketch at int_QS lines output:   
for wrt in range(2, z_QSo):
    print("RC QS ", wrt, " vane midline sketch data: \n",
          "B2B plane lengths dl_rad: \n", RC_VPS_int_QS[wrt-2][0], "\n Central angle fi coords: \n", RC_VPS_int_QS[wrt-2][1],
          "\n X coords: \n", RC_VPS_int_QS[wrt-2][2], "\n Y coords: \n", RC_VPS_int_QS[wrt-2][3], end = "\n\n")
    with open(Var_nm, "a") as f:
        f.write("RC VD" + str(wrt) + " vane midline sketch data: \n" +
          "B2B plane lengths dl_rad: \n" + str(RC_VPS_int_QS[wrt-2][0]) + "\n Central angle fi coords: \n" + str(RC_VPS_int_QS[wrt-2][1]) +
          "\n X coords: \n" + str(RC_VPS_int_QS[wrt-2][2]) + "\n Y coords: \n" + str(RC_VPS_int_QS[wrt-2][3]) + "\n\n")

    
#%% RC vane profile sketch plotting:

plt.figure(figsize = (5,5))
plt.plot(RC_VPS_Sh[2], RC_VPS_Sh[3], "bs", label = "Shroud", markersize = 4)
plt.plot(RC_VPS_H[2], RC_VPS_H[3], "rs", label = "Hub", markersize = 4)
for wre in range(2, z_QSo):
    plt.plot(RC_VPS_int_QS[wre-2][2], RC_VPS_int_QS[wre-2][3], "gs", 
             label = f"IQS nr {wre}", markersize = 4)
plt.title("RC vane sketch", fontdict = font_tit)
plt.xlabel("X", fontdict = font_tit)
plt.ylabel("Y", fontdict = font_tit)
plt.legend()


#%% Plotting meridional profile of the whole stage:

plt.figure(figsize = (5, 5))
# Impeller:
plt.plot(I_Sh[0], I_Sh[1], "bo-", markersize = 4, label = "Impeller")
plt.plot(I_H[0], I_H[1], "bo-", markersize = 4)
for hhk in range(2, z_QSo):
    plt.plot(I_int_QSC[hhk-2][0], I_int_QSC[hhk-2][1], "bo-", markersize = 4)
# Diffuser
plt.plot(D_Sh[0], D_Sh[1], "ro-", markersize = 4, label = "Diffuser")
plt.plot(D_H[0], D_H[1], "ro-", markersize = 4)
for ttj in range(2, z_QSo):
    plt.plot(D_int_QSC[ttj-2][0], D_int_QSC[ttj-2][1], "ro-", markersize = 4)
# RC:
plt.plot(RC_Sh[0], RC_Sh[1], "go-", markersize = 4, label = "RC")
plt.plot(RC_H[0], RC_H[1], "go-", markersize = 4)
for tte in range(2, z_QSo):
    plt.plot(RC_int_QSC[tte-2][0], RC_int_QSC[tte-2][1], "go-", markersize = 4)
plt.title("Stage meridional profile", fontdict = font_tit)
plt.xlabel("Axial distance", fontdict = font_tit)
plt.ylabel("Radius", fontdict = font_tit)
plt.legend()






          





 






