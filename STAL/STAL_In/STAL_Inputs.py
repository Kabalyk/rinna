# -*- coding: utf-8 -*-


class StalFileInput:
    """Class "StalFileInput" creates instances of input data necessary
        to start design calculations of a centrifugal compressor stage. 
        Input file name must be typed to the console. The required structure of
        input file is in STAL manual."""
    def __init__(self):
        # Asking for name of input file
        self.in_file = input("\n Type name of the input file: \n")
        self.properties_list = []
        # Creating empty list for reading the whole file
        self.bulk_input = []
        # Reading the whole file, each line is a separate member of list
        with open(self.in_file, "r") as inp:
            for param in inp:
                self.bulk_input.append([mem for mem in param.split("#")])
        # Sorting out the input data
        for i in range(0, len(self.bulk_input)):
            if "inlet total pressure" in self.bulk_input[i]:
                self.p_tot_in = round(float(self.bulk_input[i][0]), 0)
                self.properties_list.append("p_tot_in = " + 
                                            "{} Pa".format(self.p_tot_in))
            elif "inlet total temperature" in self.bulk_input[i]:
                self.T_tot_in = round(float(self.bulk_input[i][0]), 1)
                self.properties_list.append("T_tot_in = " + 
                                            "{} K".format(self.T_tot_in))
            elif "outlet static pressure" in self.bulk_input[i]:
                self.p_stat_out = round(float(self.bulk_input[i][0]), 0)
                self.properties_list.append("p_stat_out = " + 
                                            "{} Pa".format(self.p_stat_out))
            elif "mass flow rate" in self.bulk_input[i]:
                self.mfr = round(float(self.bulk_input[i][0]), 3)
                self.properties_list.append("mfr = " + 
                                            "{} kg/s".format(self.mfr))
            elif "medium, name" in self.bulk_input[i]:
                self.med_name = self.bulk_input[i][0]
                self.properties_list.append("medium, name = " + 
                                            "{} ".format(self.med_name))
            elif "medium, model" in self.bulk_input[i]:
                self.med_model = int(self.bulk_input[i][0])
                self.properties_list.append("medium, model = " + 
                                            "{} ".format(self.med_model))
            elif "Inlet hood" in self.bulk_input[i]:
                self.inlet_hood = int(self.bulk_input[i][0])
                self.properties_list.append("Inlet hood = " + 
                                            "{} ".format(self.inlet_hood))
            elif "IGV" in self.bulk_input[i]:
                self.igv = int(self.bulk_input[i][0])
                self.properties_list.append("IGV = " + 
                                            "{} ".format(self.igv))
            elif "Impeller" in self.bulk_input[i]:
                self.impeller = int(self.bulk_input[i][0])
                self.properties_list.append("Impeller = " + 
                                            "{} ".format(self.impeller))
            elif "Diffuser" in self.bulk_input[i]:
                self.diffuser = int(self.bulk_input[i][0])
                self.properties_list.append("Diffuser = " + 
                                            "{} ".format(self.diffuser))
            elif "RC" in self.bulk_input[i]:
                self.rc = int(self.bulk_input[i][0])
                self.properties_list.append("RC = " + 
                                            "{} ".format(self.rc))
            elif "Outlet chamber" in self.bulk_input[i]:
                self.out_cham = int(self.bulk_input[i][0])
                self.properties_list.append("Outlet chamber = " + 
                                            "{} ".format(self.out_cham))
            elif "PHI" in self.bulk_input[i]:
                self.PHI = round(float(self.bulk_input[i][0]), 3)
                self.properties_list.append("PHI = " + 
                                            "{} ".format(self.PHI))
            elif "PSI_T" in self.bulk_input[i]:
                self.PSI_T = round(float(self.bulk_input[i][0]), 3)
                self.properties_list.append("PSI_T = " + 
                                            "{} ".format(self.PSI_T))
            elif "M_u" in self.bulk_input[i]:
                self.M_u = round(float(self.bulk_input[i][0]), 3)
                self.properties_list.append("M_u = " + 
                                            "{} ".format(self.M_u))
            elif "model of losses, name" in self.bulk_input[i]:
                self.loss_model = self.bulk_input[i][0]
                self.properties_list.append("model of losses, name = " + 
                                            "{} ".format(self.loss_model))
            else:
                pass
        # Deleting now unnecessary "bulk_input" variable
        del self.bulk_input
        return
    
    def __str__(self):
        return "\n".join(self.properties_list)