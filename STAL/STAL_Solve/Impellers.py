#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module "Impellers" contains library of classes which impelement mathematical
models of centrifugal impellers. The classes are named in accordance with:
    1. Blade geometry in meridional plane (e.g. MixedFlow, Radial, etc.).
    2. Absence or presence of splitter blades (e.g. Full, Split, etc.).
    3. Author of the math model or specific name of the model (e.g. Galerkin,
    Aungier, etc.)
"""

import sys
sys.path.append("../STAL_In")
from STAL_Inputs import StalFileInput



class ImpRadialFullGalerkin(StalFileInput):
    def __init__(self):
        """
        Help on ImpRadialFullGalerkin
        """
        
        return
    def poly_efficiency(self):
        """
        Help on poly_efficiency
        """
        return
    def theor_head_coef(self):
        """
        Help on theor_head_coef
        """
        return
    def dim_geom(self):
        """
        Help on dim_geom
        """
        return
    def dim_kinem(self):
        """
        Help on dim_kinem
        """
        return
